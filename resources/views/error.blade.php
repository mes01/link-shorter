@extends('layouts.app')

@section('page')
    <div class="page page-error">
        <div class="error-404">404</div>
        <div class="message">Ссылка по которой вы перешли удалена или не существует.<div>
        <a href="/" class="link">На главную</a>
    </div>
@stop