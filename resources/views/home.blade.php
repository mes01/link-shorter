@extends('layouts.app')

@section('page')
    <div class="page page-home">
        <div class="form">
            <div class="row">
                <div class="col"><input class="shorter-url" type="url" placeholder="Ссылка для сокращения.." autofocus></div>
                <div class="col button-block"><button class="shorter-button" onclick="appShorter.set()">Сократить</button></div>
            </div>
            <div class="result-short-link"></div>
        </div>
    </div>
@stop