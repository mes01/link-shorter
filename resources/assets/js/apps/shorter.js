var appShorter = {

    //выровнять блок на странице
    positionBlock: function () {
        var heightBlock = $(".page.page-home").find(".form").outerHeight();
        var heightPage = $("body").height();

        $(".page.page-home").find(".form").css("top", ((heightPage/2)-(heightBlock/2)-60) );
    },

    //сгенерировать ссылку
    set: function () {
        var link = $(".shorter-url").val()
        if(/(http:\/\/|https:\/\/|)([a-zA-Zа-яА-Я0-9\_\-\.]{2,255})\.([a-zA-Zа-яА-ЯA-Z0-9]{2,255})(\/)*/i.test(link) == false){
            $(".result-short-link").show().text("Ссылка указана не верно");
            return false;
        }

        $.ajax({
            url: "/short",
            method: "post",
            beforeSend: function () {
                $(".shorter-button")
                    .prop("disabled", true)
                    .html('<i class="fa fa-spinner fa-spin"></i>');
            },
            data: { link: link },
            success: function (data) {
                if(data.status == "ok"){
                    $(".result-short-link").show().text("http://localhost:8000/"+data.code);
                }else{
                    $(".result-short-link").show().text(data.desc);
                    console.error(data.desc);
                }
            },
            complete: function () {
                $(".shorter-button")
                    .prop("disabled", false)
                    .html('Сократить');
            }
        });
    }
}

$(function () {
    appShorter.positionBlock();

    $('.shorter-url').keydown(function(event) {
        if((event.keyCode || event.which) == 13) {
            event.preventDefault();
            appShorter.set();
            return false;
        }
    });

    $(window).resize(function () {
        appShorter.positionBlock();
    });
});