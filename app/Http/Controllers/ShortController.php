<?php

namespace App\Http\Controllers;

use DB;
use Request;
use App\Http\Requests;

class ShortController extends Controller
{
	//!получить ссылку
	public function get($code){
		$getLink = DB::table('links')
			->where('code', '=', $code)
			->get();

		if(count($getLink) == 1){
			$link = $getLink[0]->link;
			if(!preg_match("/(http|https)/i", $link)) $link = "http://".$link;

			$update = DB::table('links')
				->where('code', '=', $link)
				->increment('redirect_count', 1);
			//localhost:8000/7nkUKTHaVl
			return redirect($link);
		}else{
			return view("error", ['code'=>$code]);
		}
	}

	//!сгенерировать ссылку
    public function set(Request $request){

    	$link = Request::input('link');
		if(!preg_match('/(http:\/\/|https:\/\/|)([a-zA-Zа-яА-Я0-9\_\-\.]{2,255})\.([a-zA-Zа-яА-ЯA-Z0-9]{2,255})(\/)*/iu', $link)){
			$respouse = [
				'status' => 'error',
				'desc' => 'Указана не правильная ссылка'
			];
			return response()->json($respouse);
		}

		$getLink = DB::table('links')->where('link', '=', $link)->get();

		//есть ли ссылка в базе
		if(count($getLink) > 0){
			$respouse = [
				'status' => 'ok',
				'code' => $getLink[0]->code
			];
		}else{
			$code = $this->generateCode();

			try{
				DB::table('links')->insert([
					"code" => $code,
					"link" => $link
				]);
				$respouse = [
					'status' => 'ok',
					'code' => $code
				];
			} catch(\Illuminate\Database\QueryException $ex){

				//если так случилось что код ссылки совпал с уже существующим
				//вероятность совпадения 62 в 10 степени к количеству ссылок в базе
				//очень маловероятно, но мало ли :) по этому catch.

				if(preg_match("/(PRIMARY)/i", $ex->getMessage())){
					$code = $this->generateCode();
					DB::table('links')->insert([
						"code" => $code,
						"link" => $link
					]);
					$respouse = [
						'status' => 'ok',
						'code' => $code
					];
				}else{
					$respouse = ['status' => 'Что-то пошло не так, попробуйте повторить запрос позже.'];
				}
			}
		}

		return response()->json($respouse);
	}

	//генерация кода ссылки
	private function generateCode($length = 10) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$count = mb_strlen($chars);

		for ($i = 0, $result = ''; $i < $length; $i++) {
			$index = rand(0, $count - 1);
			$result .= mb_substr($chars, $index, 1);
		}

		return $result;
	}

}
