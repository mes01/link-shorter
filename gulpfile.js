var gulp = require("gulp");
var shell = require("gulp-shell");
var autoprefixer = require('gulp-autoprefixer');
var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function(mix) {
    mix.sass(["app.scss"], "public/css/app.css");
    mix.scripts([
        "utils/*.js",
        "config.js",
        "apps/*.js",
    ], 'public/js/app.js');
});
